#pragma once

#include <sstream>
#include <iostream>
#include "md5/md5.h"


std::string getBEGUID(std::string steamid) {

	std::stringstream beguid;


	__int64 steamID = std::stoll(steamid);
	__int8 i = 0, parts[8] = { 0 };

	beguid << "BE";
	do { parts[i++] = steamID & 0xFF; } while (steamID >>= 8);

	for (int i = 0; i < sizeof(parts); i++)
		beguid << char(parts[i]);

	std::cout << md5(beguid.str()) << std::endl << std::endl;
	return md5(beguid.str());
}