#include <iostream>
#include <Windows.h>
#include "beguid.h"


void toClipboard(const std::string &s) {
	OpenClipboard(0);
	EmptyClipboard();
	HGLOBAL hg = GlobalAlloc(GMEM_MOVEABLE, s.size()+1);
	if (!hg) {
		CloseClipboard();
		return;
	}
	memcpy(GlobalLock(hg), s.c_str(), s.size()+1);
	GlobalUnlock(hg);
	SetClipboardData(CF_TEXT, hg);
	CloseClipboard();
	GlobalFree(hg);
}

std::string GetClipboardText()
{
	// Try opening the clipboard
	if (!OpenClipboard(nullptr))
		std::cout << "There was an error processing the clipboard content!" << std::endl;

			// Get handle of clipboard object for ANSI text
		HANDLE hData = GetClipboardData(CF_TEXT);
	if (hData == nullptr)
		std::cout << "There was an error processing the clipboard content!" << std::endl;

	// Lock the handle to get the actual text pointer
	char * pszText = static_cast<char*>(GlobalLock(hData));

	if (pszText == nullptr)
		std::cout << "There was an error processing the clipboard content!" << std::endl;

	// Save text in a string class instance
	std::string text(pszText);

	// Release the lock
	GlobalUnlock(hData);

	// Release the clipboard
	CloseClipboard();

	return text;
}

int main() {

	std::string steamid;

	std::cout << "This little tool converts a Steam ID to a BE GUID as used by BEC." << std::endl << std::endl << 
		"Please copy the Steam ID you need to convert to the clipboard (Ctrl+C)." << std::endl <<
		"Then hit 'Enter'." << std::endl << std::endl <<
		"The converted BE GUID will be copied to your clipboard automatically." << std::endl;

	system("PAUSE");

	steamid = GetClipboardText();

	if (steamid.length() != 17) {
		std::cout << "Please use a valid Steam ID!" << std::endl << std::endl;
		system("PAUSE");
		return -1;
	}
	std::cout << std::endl << std::endl;
	toClipboard(getBEGUID(steamid));
	system("PAUSE");
	return 0;
}