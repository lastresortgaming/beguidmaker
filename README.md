Convert SteamIDs to BE GUIDs in seconds.

Easy to use, hard to break. Uses the clipboard for handling I/O, no typing required!

Written by Mokka. (c) 2017